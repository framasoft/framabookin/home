[![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/48px-GitLab_Logo.svg.png)](https://framagit.org)

🇬🇧 **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

🇫🇷 **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

Framabookin est une bibliothèque de livres libres ou du domaine public que Framasoft propose sur le site : https://framabookin.org

La bibliothèque est gérée par les logiciels [Calibre](https://calibre-ebook.com/) et [BicBucStriim](https://github.com/rvolz/BicBucStriim/releases).  
Pour en savoir plus sur l'installation, voir le tutoriel sur Framacloud.

La base de données Calibre se trouve dans [ce dépôt](https://framagit.org/framasoft/framabookin/db)
